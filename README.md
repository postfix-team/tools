# tools

Scripts used for Debian packaging of Postfix.

There are currently three (and they are primarliy for managing postfix in git):

*  formatChangelog (make draft d/changelog from git commits)
*  wietse-postfix (import new upstream release)
*  tagRelease (tag a Debian release)

These were originally written by Lamont Jones and are licensed under the same
terms as the Debian packaging of Postfix.

They are not intended to be generally useful for anyone but the postfix
maintainers.
