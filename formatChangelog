#!/usr/bin/python3
import os
import pwd
import sys

distroCloses={
    'debian': ('  Closes: #%s', ", #"),
    'ubuntu': ('  LP: #%s', ", #"),
    'cve:':    ('  %s', ", ")
}

class Commit:
  def __init__(self,lines):
    fresh=True
    self.bugs={}
    self.ismerge=False
    self.log=""
    self.author=None
    for l in lines:
      if l.startswith("commit "):
        continue
      elif l.startswith("Merge: "):
        self.ismerge=True
        continue
      elif l.startswith("Author: "):
        self.author=l[8:].split('<')[0].strip()
      elif l.startswith("        ") and len(l.strip()):
        l=l[4:]
      elif l.startswith("    ") and len(l.strip()):
        l=l[4:]
        if fresh and (l.startswith("Merge commit") or \
            l.startswith("Merge branch") or \
            l.startswith("changelog")) and \
            not (l.find("Closes: #")>=0 or l.find("LP: #")>=0):
          fresh=False
        if fresh:
          fresh=False
          self.log=l.strip()
        elif not fresh and l.startswith("Addresses-") and \
            (l.find("-Bug:")>0 or l.find("-CVE:")):
          distro=l.split()[0].split('-')[1].lower()
          self.bugs.setdefault(distro,[])
          for b in l.split()[1:]:
            for b2 in b.split(','):
              if len(b2) and b2[0]=='#':
                b2=b2[1:]
              if len(b2):
                self.bugs[distro].append(b2)
        # Addresses: http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=510130
        elif not fresh and l.startswith("Addresses:") and \
            l.find("bugs.debian.org")>0:
          distro = "debian"
          self.bugs.setdefault(distro,[])
          if l.find("?bug=") >= 0:
              b = l[l.find("?bug=")+5:-1]
              self.bugs[distro].append(b)
          else:
              b = l[l.rfind("/")+1:-1]
              self.bugs[distro].append(b)
  def __str__(self):
    ret=self.log
    if not len(self.bugs):
      return ret
    if not ret.endswith('.'):
      ret+='.'
    for d in list(self.bugs.keys()):
      fmt1, fmt2 = distroCloses.get(d,("  %s: #%%s" % (d), ", #"))
      ret += fmt1 % ( fmt2.join(self.bugs[d]))
    return ret

  def __eq__(self,other):
    return self.__str__()==other.__str__()
  def __ne__(self,other):
    return self.__str__()!=other.__str__()

class Author:
  def __init__(self,author):
    self.name=author
    self.commits=[]
    self.po_commits = []
  def addCommit(self,commit):
    if commit.__str__().startswith("po: ") or commit.__str__().startswith("l10n: "):
      targ = self.po_commits
    else:
      targ = self.commits
    if len(targ)==0 or targ[-1]!=commit:
      targ.append(commit)
  def fold_changelog(self,line):
    ret = ""
    t="  * %s" % line
    while len(t) > 0:
      space = len(t)
      if space > 79:
        space = t[:79].rfind(' ')
        if t[:space].endswith("Closes:"):
            space=t[:space].rfind(' ')
        if space < 0:
            space = len(t)
        ret += t[:space]+'\n'
        t = "    "+t[space+1:]
      else:
        ret += t+'\n'
        t=""
    return ret
  def po_changelog(self,multi):
    ret=""
    if len(self.commits) == 0:
      for c in self.po_commits:
        ret += self.fold_changelog("%s (%s)" % (c, self.name))
    return ret
  def changelog(self,multi):
    ret=""
    if len(self.commits):
      if multi:
        ret+="\n  [%s]\n\n"%(self.name)
      logs = self.po_commits + self.commits
      logs.reverse()
      for v in logs:
        if len(str(v)):
            ret += self.fold_changelog(v)
    return ret

def main():
  global authors	# XXX
  authors={}
  if len(sys.argv)>1:
    version=sys.argv[1]
  else:
    version=os.popen("git describe --tags --candidates=50").readline().strip()
    # strip off the -<count>-g<id> if present
    vsplit=version.split('-')
    if len(vsplit)>2 and vsplit[-2].isdigit() and vsplit[-1][0]=='g' and \
    	vsplit[-1][1:].isalnum():
      version="-".join(vsplit[:-2])

  cmd="git log --pretty=medium %s..HEAD"%version
  lines=os.popen(cmd ).readlines()
  start,end=0,len(lines)
  while start < end:
    pos=start+1
    while pos<end and not lines[pos].startswith("commit"):
      pos+=1
    c=Commit(lines[start:pos])
    authors.setdefault(c.author,Author(c.author)).addCommit(c)
    start=pos
  myname = 'Scott Kitterman'
  multiAuthor=len(authors)>1 or not myname in authors
  changes=""
  po_changes = ""
  for k in list(authors.keys()):
    changes+=authors[k].changelog(multiAuthor)
    po_changes += authors[k].po_changelog(multiAuthor)
  print(changes[multiAuthor:-1])
  if len(changes[multiAuthor:-1]):
    print()
  if len(po_changes):
    print("  [localization folks]\n")
    print(po_changes[:-1])

if __name__=="__main__":
  main()
